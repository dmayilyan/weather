Funktionen:
- aktuelles Wetter
- 7-Tage-Vorhersage
- 48-Stunden-Vorhersage
- Wetterkarten
- ändern der Stadt
- mehrere Städte
- mehrere Einheiten
- kleiner als 5MB

Unterstützte Sprachen:
- Englisch
- Deutsch
- Finnisch
- Spanisch
- Portugiesisch
- Serbisch
- Russisch
- Griechisch
- Georgisch

Wetterdaten:
Die Wetterdaten werden durch <a href="https://openweathermap.org/">OpenWeatherMap.org</a>, lizenziert unter der <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a> bereitgestellt.

Einen API-Schlüssel erhalten:
Sie müssen sich bei <a href="https://home.openweathermap.org/users/sign_up">OpenWeatherMap.org</a> kostenlos anmelden. Es kann eine Weile dauern, bis der API-Key aktiviert ist!

Lizenzierung:
Lizenziert unter der <a href="https://gitlab.com/BeowuIf/weather/-/blob/master/LICENSE">EUPL-1.2</a>.

OpenSource:
Diese Anwendung ist <a href="https://gitlab.com/BeowuIf/weather">OpenSource</a>.

Wiki:
Sie haben Fragen, brauchen Hilfe beim Einrichten der App oder wollen einfach mehr darüber wissen? Dann werfen Sie einen Blick in das <a href="https://gitlab.com/BeowuIf/weather/-/wikis/home">Wiki</a>.
