Features:
- current weather
- 7 day forecast
- 48 hour forecast
- weather maps
- changing the city
- multiple cities
- multiple units
- smaller than 5MB

Supported languages:
- English
- German
- Finnish
- Spanish
- Portuguese
- Serbian
- Russian
- Greek
- Georgian

Weather data:
Weather data is provided by <a href="https://openweathermap.org/">OpenWeatherMap.org</a>, licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>.

Get an API-Key:
Go to <a href="https://home.openweathermap.org/users/sign_up">OpenWeatherMap.org</a> and sign up for free. It may take a while before the API-Key is activated!

Licensing:
Licensed under the <a href="https://gitlab.com/BeowuIf/weather/-/blob/master/LICENSE">EUPL-1.2</a>.

OpenSource:
This app is <a href="https://gitlab.com/BeowuIf/weather">open source</a>.

Wiki:
You have questions, need help to set up the app or simply want to know more about it? Then take a look at the <a href="https://gitlab.com/BeowuIf/weather/-/wikis/home">wiki</a>.
