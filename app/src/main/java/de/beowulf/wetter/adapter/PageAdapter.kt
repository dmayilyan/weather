package de.beowulf.wetter.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import de.beowulf.wetter.fragments.*

class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount(): Int {
            return 5
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> GraphDayFragment()
            1 -> DayFragment()
            3 -> HourFragment()
            4 -> GraphHourFragment()
            else -> MainFragment()
        }
    }
}