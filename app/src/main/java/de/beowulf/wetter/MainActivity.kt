package de.beowulf.wetter

import android.os.Bundle
import android.util.TypedValue
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import de.beowulf.wetter.adapter.PagerAdapter
import org.jetbrains.anko.backgroundColor

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(GlobalFunctions().getTheme(this))
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!GlobalFunctions().gradient(this)) {
            val typedValue = TypedValue()
            theme.resolveAttribute(R.attr.colorPrimaryDark, typedValue, true)
            val root = findViewById<RelativeLayout>(R.id.Main)
            root.backgroundColor = typedValue.data
        }

        val viewPager = findViewById<ViewPager>(R.id.pager)
        val adapter = PagerAdapter(
            supportFragmentManager
        )
        viewPager.adapter = adapter

        viewPager.currentItem = 2
    }

    override fun onBackPressed() {
        val viewPager = findViewById<ViewPager>(R.id.pager)
        if (viewPager.currentItem != 2) {
            if (viewPager.currentItem < 2)
                viewPager.currentItem ++
            else
                viewPager.currentItem --
        } else {
            super.onBackPressed()
        }
    }
}