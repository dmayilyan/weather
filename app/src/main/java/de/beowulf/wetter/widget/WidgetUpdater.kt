package de.beowulf.wetter.widget

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import java.util.*

class WidgetUpdater(private val mContext: Context) {
    private val alarmID = 0
    fun startAlarm() {
        val calendar = Calendar.getInstance()
        val intervalMillis = 900000
        calendar.add(Calendar.MILLISECOND, intervalMillis)
        val alarmIntent = Intent(mContext, WidgetProvider::class.java)
        alarmIntent.action = WidgetProvider.ACTION_AUTO_UPDATE
        val pendingIntent = PendingIntent.getBroadcast(
            mContext,
            alarmID,
            alarmIntent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
        val alarmManager = mContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        // RTC does not wake the device up
        alarmManager.setRepeating(
            AlarmManager.RTC,
            calendar.timeInMillis,
            intervalMillis.toLong(),
            pendingIntent
        )
    }

    fun stopAlarm() {
        val alarmIntent = Intent(mContext, WidgetProvider::class.java)
        alarmIntent.action = WidgetProvider.ACTION_AUTO_UPDATE
        val pendingIntent = PendingIntent.getBroadcast(
            mContext,
            alarmID,
            alarmIntent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
        val alarmManager = mContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.cancel(pendingIntent)
    }
}