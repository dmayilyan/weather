package de.beowulf.wetter.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import de.beowulf.wetter.GlobalFunctions
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.util.concurrent.Executors
import javax.net.ssl.HttpsURLConnection

open class WidgetProvider : AppWidgetProvider() {

    private val gf = GlobalFunctions()

    override fun onReceive(context: Context, intent: Intent) {

        gf.initializeContext(context)

        if (gf.getInitialized()) {
            if (ACTION_AUTO_UPDATE == intent.action) {
                val executor = Executors.newScheduledThreadPool(5)

                context.doAsync(executorService = executor) {
                    val result: String? = try {
                        with(URL(gf.url("normal", "")).openConnection() as HttpsURLConnection) {
                            sslSocketFactory = gf.getSocketFactory()
                            requestMethod = "GET"
                            BufferedReader(InputStreamReader(inputStream)).readText()
                        }
                    } catch (e: Exception) {
                        null
                    }
                    uiThread {
                        if (result != null) {
                            gf.setResult(result)
                        }
                        updateWidgets(context)
                    }
                }
            } else if ("android.app.action.NEXT_ALARM_CLOCK_CHANGED" == intent.action ||
                    "android.appwidget.action.APPWIDGET_ENABLED" == intent.action){
                updateWidgets(context)
                super.onReceive(context, intent)
            } else {
                super.onReceive(context, intent)
            }
        }
    }

    fun updateWidgets(context: Context) {
        sendIntents(context, AppWidget::class.java)
        sendIntents(context, TimeWidget::class.java)
        sendIntents(context, HourForecastWidget::class.java)
        sendIntents(context, DayForecastWidget::class.java)
    }

    private fun sendIntents(context: Context, widgetClass: Class<*>) {
        val intent = Intent(context.applicationContext, widgetClass)
            .setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
        val ids = AppWidgetManager.getInstance(context.applicationContext)
            .getAppWidgetIds(ComponentName(context.applicationContext, widgetClass))
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
        context.applicationContext.sendBroadcast(intent)
    }

    override fun onDisabled(context: Context) {

        val appWidgetManager = AppWidgetManager.getInstance(context)

        val allWidgetIds =
            appWidgetManager.getAppWidgetIds(
                ComponentName(
                    context,
                    AppWidget::class.java
                )
            ).size +
            appWidgetManager.getAppWidgetIds(
                ComponentName(
                    context,
                    TimeWidget::class.java
                )
            ).size +
            appWidgetManager.getAppWidgetIds(
                ComponentName(
                    context,
                    DayForecastWidget::class.java
                )
            ).size +
            appWidgetManager.getAppWidgetIds(
                ComponentName(
                    context,
                    HourForecastWidget::class.java
                )
            ).size
        if (allWidgetIds == 0) {
            // stop alarm
            val appWidgetAlarm = WidgetUpdater(context.applicationContext)
            appWidgetAlarm.stopAlarm()

        }
    }

    override fun onEnabled(context: Context) {
        val appWidgetAlarm = WidgetUpdater(context.applicationContext)
        appWidgetAlarm.startAlarm()
    }

    companion object {
        @JvmField
        var ACTION_AUTO_UPDATE: String = "AUTO_UPDATE"
    }
}