package de.beowulf.wetter.widget

import android.app.AlarmManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock
import android.util.TypedValue
import android.view.View
import android.widget.RemoteViews
import androidx.core.content.ContextCompat.getSystemService
import de.beowulf.wetter.GlobalFunctions
import de.beowulf.wetter.R
import de.beowulf.wetter.StartActivity
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class TimeWidget : WidgetProvider() {

    private val gf = GlobalFunctions()

    override fun onAppWidgetOptionsChanged(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int,
        newOptions: Bundle
    ) {
        gf.initializeContext(context)
        val views = RemoteViews(context.packageName, R.layout.time_widget)

        resizeWidget(newOptions, views)

        //Start app, when you click on the widget and when API level 19 or higher, open Alarm Settings, when you click on the clock
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            val configIntent = Intent(AlarmClock.ACTION_SHOW_ALARMS)
            val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
            views.setOnClickPendingIntent(R.id.Clock, configPendingIntent)
        }
        val configIntent = Intent(context, StartActivity::class.java)
        val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
        views.setOnClickPendingIntent(R.id.TimeWidget, configPendingIntent)

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views)
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        val gf = GlobalFunctions()
        gf.initializeContext(context)
        val views = RemoteViews(context.packageName, R.layout.time_widget)

        if (gf.getInitialized()) {
            val jsonObj = gf.result()

            //get data
            val current: JSONObject = jsonObj.getJSONObject("current")
            val currentWeather: JSONObject = current.getJSONArray("weather").getJSONObject(0)

            val nextAlarm: String? =
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP && getSystemService(
                        context,
                        AlarmManager::class.java
                    )?.nextAlarmClock?.triggerTime != null
                ) {
                    SimpleDateFormat("E, ${gf.getTime()}", Locale.getDefault()).format(
                        Date(
                            getSystemService(
                                context,
                                AlarmManager::class.java
                            )!!.nextAlarmClock.triggerTime
                        )
                    )
                } else {
                    null
                }
            val image: Int = gf.icon(currentWeather.getString("icon"))
            val actualTemp: String = gf.convertTemp(current.getDouble("temp"))
            val statusText: String = currentWeather.getString("description").replace(" ", "\n")
            val wind: String =
                gf.convertSpeed(current.getDouble("wind_speed")) + " (${
                    gf.degToCompass(current.getInt("wind_deg"))
                })"

            var rainSnow: String
            var type = "snow"
            when {
                current.has("rain") -> {
                    rainSnow = gf.convertRain(current.getJSONObject("rain").getDouble("1h"))
                    type = "rain"
                }
                current.has("snow") -> {
                    rainSnow = gf.convertRain(current.getJSONObject("snow").getDouble("1h"))
                }
                else -> {
                    rainSnow = gf.convertRain(0.0)
                    type = "rain"
                }
            }
            if (type == "rain") {
                views.setViewVisibility(R.id.Rain, View.VISIBLE)
                views.setViewVisibility(R.id.Snow, View.GONE)
            } else {
                views.setViewVisibility(R.id.Rain, View.GONE)
                views.setViewVisibility(R.id.Snow, View.VISIBLE)
            }
            val precipitation: Double =
                jsonObj.getJSONArray("hourly").getJSONObject(0).getDouble("pop") * 100
            rainSnow += " (${precipitation.toString().split(".")[0]}%)"

            for (appWidgetId in appWidgetIds) {

                //set View
                if (nextAlarm == null) {
                    views.setViewVisibility(R.id.AlarmIcon, View.GONE)
                    views.setViewVisibility(R.id.nextAlarm, View.GONE)
                } else {
                    views.setViewVisibility(R.id.AlarmIcon, View.VISIBLE)
                    views.setViewVisibility(R.id.nextAlarm, View.VISIBLE)
                    views.setTextViewText(R.id.nextAlarm, nextAlarm)
                }
                views.setImageViewResource(R.id.Status_Image, image)
                views.setTextViewText(R.id.actualTemp, actualTemp)
                views.setTextViewText(R.id.Status_Text, statusText)
                views.setTextViewText(R.id.Wind, wind)
                views.setTextViewText(R.id.RainSnow, rainSnow)

                val appWidgetOptions = appWidgetManager.getAppWidgetOptions(appWidgetId)
                resizeWidget(appWidgetOptions, views)

                //Start app, when you click on the widget and when API level 19 or higher, open Alarm Settings, when you click on the clock
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    val configIntent = Intent(AlarmClock.ACTION_SHOW_ALARMS)
                    val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
                    views.setOnClickPendingIntent(R.id.Clock, configPendingIntent)
                }
                val configIntent = Intent(context, StartActivity::class.java)
                val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
                views.setOnClickPendingIntent(R.id.TimeWidget, configPendingIntent)

                // Instruct the widget manager to update the widget
                appWidgetManager.updateAppWidget(appWidgetId, views)
            }
        }
    }
}

private fun resizeWidget(appWidgetOptions: Bundle, views: RemoteViews) {
    val minWidth: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH)
    /*val maxWidth: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH)
    val minHeight: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)*/
    val maxHeight: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)

    when {
        minWidth < 250 -> {
            views.setViewVisibility(R.id.WindRain, View.GONE)

        }
        else -> {
            views.setViewVisibility(R.id.WindRain, View.VISIBLE)
        }
    }
    when {
        maxHeight < 110 -> {
            views.setTextViewTextSize(R.id.Clock, TypedValue.COMPLEX_UNIT_SP, 45F)
        }
        maxHeight > 250 && minWidth > 250 -> {
            views.setTextViewTextSize(R.id.Clock, TypedValue.COMPLEX_UNIT_SP, 100F)
            views.setTextViewTextSize(R.id.dayTextClock, TypedValue.COMPLEX_UNIT_SP, 15F)
            views.setTextViewTextSize(R.id.nextAlarm, TypedValue.COMPLEX_UNIT_SP, 15F)
            views.setViewVisibility(R.id.Space, View.VISIBLE)
        }
        maxHeight > 110 -> {
            views.setTextViewTextSize(R.id.Clock, TypedValue.COMPLEX_UNIT_SP, 70F)
            views.setTextViewTextSize(R.id.dayTextClock, TypedValue.COMPLEX_UNIT_SP, 12F)
            views.setTextViewTextSize(R.id.nextAlarm, TypedValue.COMPLEX_UNIT_SP, 12F)
            views.setViewVisibility(R.id.Space, View.GONE)
        }
    }
}
