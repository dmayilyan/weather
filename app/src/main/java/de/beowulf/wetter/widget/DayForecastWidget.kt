package de.beowulf.wetter.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RemoteViews
import de.beowulf.wetter.GlobalFunctions
import de.beowulf.wetter.R
import de.beowulf.wetter.StartActivity
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class DayForecastWidget : WidgetProvider() {

    private val gf = GlobalFunctions()

    override fun onAppWidgetOptionsChanged(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int,
        newOptions: Bundle
    ) {
        gf.initializeContext(context)
        val views = RemoteViews(context.packageName, R.layout.forecast_widget)

        resizeWidget(newOptions, views)
        //Start app, when you click on the widget
        val configIntent = Intent(context, StartActivity::class.java)
        val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
        views.setOnClickPendingIntent(R.id.forecast_widget, configPendingIntent)

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views)
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        val gf = GlobalFunctions()
        val views = RemoteViews(context.packageName, R.layout.forecast_widget)

        gf.initializeContext(context)

        val day = arrayOfNulls<String>(8)
        val image = arrayOfNulls<Int>(8)
        val maxTemp = arrayOfNulls<String>(8)
        val minTemp = arrayOfNulls<String>(8)

        val jsonObj = gf.result()

        for (i: Int in 1..7) {
            val daily: JSONObject = jsonObj.getJSONArray("daily").getJSONObject(i)
            val dailyTemp: JSONObject = daily.getJSONObject("temp")
            val dailyWeather: JSONObject = daily.getJSONArray("weather").getJSONObject(0)

            day[i] = SimpleDateFormat(context.getString(R.string.daydate), Locale.getDefault()).format(
                    Date(
                        daily.getLong("dt") * 1000
                    )
                )

            image[i] = gf.icon(dailyWeather.getString("icon"))

            maxTemp[i] = gf.convertTemp(dailyTemp.getDouble("max"))
            minTemp[i] = gf.convertTemp(dailyTemp.getDouble("min"))
        }

        for (appWidgetId in appWidgetIds) {

            //set View
            // next item:
            views.setTextViewText(R.id.day1, day[1])
            image[1]?.let {views.setImageViewResource(R.id.dayImage1, it)}
            views.setTextViewText(R.id.dayMax1, maxTemp[1])
            views.setTextViewText(R.id.dayMin1, minTemp[1])
            // next item:
            views.setTextViewText(R.id.day2, day[2])
            image[2]?.let {views.setImageViewResource(R.id.dayImage2, it)}
            views.setTextViewText(R.id.dayMax2, maxTemp[2])
            views.setTextViewText(R.id.dayMin2, minTemp[2])
            // next item:
            views.setTextViewText(R.id.day3, day[3])
            image[3]?.let {views.setImageViewResource(R.id.dayImage3, it)}
            views.setTextViewText(R.id.dayMax3, maxTemp[3])
            views.setTextViewText(R.id.dayMin3, minTemp[3])
            // next item:
            views.setTextViewText(R.id.day4, day[4])
            image[4]?.let {views.setImageViewResource(R.id.dayImage4, it)}
            views.setTextViewText(R.id.dayMax4, maxTemp[4])
            views.setTextViewText(R.id.dayMin4, minTemp[4])
            // next item:
            views.setTextViewText(R.id.day5, day[5])
            image[5]?.let {views.setImageViewResource(R.id.dayImage5, it)}
            views.setTextViewText(R.id.dayMax5, maxTemp[5])
            views.setTextViewText(R.id.dayMin5, minTemp[5])
            // next item:
            views.setTextViewText(R.id.day6, day[6])
            image[6]?.let {views.setImageViewResource(R.id.dayImage6, it)}
            views.setTextViewText(R.id.dayMax6, maxTemp[6])
            views.setTextViewText(R.id.dayMin6, minTemp[6])
            // next item:
            views.setTextViewText(R.id.day7, day[7])
            image[7]?.let {views.setImageViewResource(R.id.dayImage7, it)}
            views.setTextViewText(R.id.dayMax7, maxTemp[7])
            views.setTextViewText(R.id.dayMin7, minTemp[7])

            val appWidgetOptions = appWidgetManager.getAppWidgetOptions(appWidgetId)
            resizeWidget(appWidgetOptions, views)

            //Start app, when you click on the widget
            val configIntent = Intent(context, StartActivity::class.java)
            val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
            views.setOnClickPendingIntent(R.id.forecast_widget, configPendingIntent)

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}

private fun resizeWidget(appWidgetOptions: Bundle, views: RemoteViews) {
    val minWidth: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH)
    /*val maxWidth: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH)
    val minHeight: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)
    val maxHeight: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)*/
    //first make all gone
    views.setViewVisibility(R.id.item2, View.GONE)
    views.setViewVisibility(R.id.item3, View.GONE)
    views.setViewVisibility(R.id.item4, View.GONE)
    views.setViewVisibility(R.id.item5, View.GONE)
    views.setViewVisibility(R.id.item6, View.GONE)
    views.setViewVisibility(R.id.item7, View.GONE)

    when {
        minWidth < 120 -> {
        }
        minWidth < 180 -> {
            views.setViewVisibility(R.id.item2, View.VISIBLE)
        }
        minWidth < 240 -> {
            views.setViewVisibility(R.id.item2, View.VISIBLE)
            views.setViewVisibility(R.id.item3, View.VISIBLE)
        }
        minWidth < 300 -> {
            views.setViewVisibility(R.id.item2, View.VISIBLE)
            views.setViewVisibility(R.id.item3, View.VISIBLE)
            views.setViewVisibility(R.id.item4, View.VISIBLE)
        }
        minWidth < 360 -> {
            views.setViewVisibility(R.id.item2, View.VISIBLE)
            views.setViewVisibility(R.id.item3, View.VISIBLE)
            views.setViewVisibility(R.id.item4, View.VISIBLE)
            views.setViewVisibility(R.id.item5, View.VISIBLE)
        }
        minWidth < 420 -> {
            views.setViewVisibility(R.id.item2, View.VISIBLE)
            views.setViewVisibility(R.id.item3, View.VISIBLE)
            views.setViewVisibility(R.id.item4, View.VISIBLE)
            views.setViewVisibility(R.id.item5, View.VISIBLE)
            views.setViewVisibility(R.id.item6, View.VISIBLE)
        }
        minWidth < 480 -> {
            views.setViewVisibility(R.id.item2, View.VISIBLE)
            views.setViewVisibility(R.id.item3, View.VISIBLE)
            views.setViewVisibility(R.id.item4, View.VISIBLE)
            views.setViewVisibility(R.id.item5, View.VISIBLE)
            views.setViewVisibility(R.id.item6, View.VISIBLE)
            views.setViewVisibility(R.id.item7, View.VISIBLE)
        }
    }
}
