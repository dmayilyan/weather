package de.beowulf.wetter

import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.util.TypedValue
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import de.beowulf.wetter.databinding.ActivityStartBinding
import de.beowulf.wetter.widget.WidgetProvider
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.util.concurrent.Executors
import javax.net.ssl.HttpsURLConnection

class StartActivity : AppCompatActivity() {

    private lateinit var binding: ActivityStartBinding

    private val gf = GlobalFunctions()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(gf.getTheme(this))
        super.onCreate(savedInstanceState)
        binding = ActivityStartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (!gf.gradient(this)) {
            val typedValue = TypedValue()
            theme.resolveAttribute(R.attr.colorPrimaryDark, typedValue, true)
            binding.root.backgroundColor = typedValue.data
        }

        gf.initializeContext(this)

        val settings: SharedPreferences = getSharedPreferences("de.beowulf.wetter", 0)

        binding.CreatedBy.movementMethod = LinkMovementMethod.getInstance()
        binding.ReportError.movementMethod = LinkMovementMethod.getInstance()
        binding.OWM.movementMethod = LinkMovementMethod.getInstance()

        var lat: String = settings.getString("lat", "")!!
        var city: String = settings.getString("city", "")!!
        var api: String = settings.getString("api", "")!!
        val change: Boolean = intent.getBooleanExtra("change", false)

        if (change) {
            binding.City.setText(city)
            if (api != getString(R.string.standardKey))
                binding.Api.setText(api)
        }

        binding.ApiText.setOnClickListener {
            val uriUrl: Uri = Uri.parse("https://home.openweathermap.org/api_keys")
            val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
            startActivity(launchBrowser)
        }

        binding.UISettings.setOnClickListener {
            changeSettings()
        }

        if (lat != "" && !change) { // When already initialized load data and start the MainActivity
            binding.loader.visibility = View.VISIBLE
            binding.mainContainer.visibility = View.GONE

            startMain(false)

        } else binding.Submit.setOnClickListener { // When not already initialized, set Submit-Button OnClickListener and check the data
            binding.loader.visibility = View.VISIBLE
            binding.mainContainer.visibility = View.GONE
            val executor = Executors.newScheduledThreadPool(5)

            doAsync(executorService = executor) {
                var error = 3
                val result: JSONArray = try {
                    city = binding.City.text.toString()
                    api = binding.Api.text.toString()
                    if (api == "")
                        api = getString(R.string.standardKey)

                    with(URL("https://api.openweathermap.org/geo/1.0/direct?q=$city&limit=10&appid=$api").openConnection() as HttpsURLConnection) {
                        sslSocketFactory = gf.getSocketFactory()
                        requestMethod = "GET"
                        error = when (responseCode) {
                            429, 401 -> {
                                1
                            }
                            404 -> {
                                2
                            }
                            else -> {
                                0
                            }
                        }
                        JSONArray(BufferedReader(InputStreamReader(inputStream)).readText())
                    }
                } catch (e: Exception) {
                    JSONArray("[]")
                }
                uiThread {
                    if (error == 0 && result.length() != 0) {
                        var i = 0
                        val cities = arrayOfNulls<String>(result.length())
                        while (i < result.length())
                        {
                            if (result.getJSONObject(i).has("state")) {
                                cities[i] = result.getJSONObject(i).getString("name") +
                                        ", " + result.getJSONObject(i).getString("state") +
                                        ", " + result.getJSONObject(i).getString("country")
                            } else {
                                cities[i] = result.getJSONObject(i).getString("name") +
                                        ", " + result.getJSONObject(i).getString("country")
                            }
                            i++
                        }

                        AlertDialog.Builder(
                            ContextThemeWrapper(
                                this@StartActivity,
                                R.style.AlertDialog
                            )
                        )
                            .setTitle(R.string.choose_city)
                            .setItems(cities) { _, selected ->
                                city = cities[selected].toString()
                                lat = result.getJSONObject(selected).getString("lat")
                                val lon: String = result.getJSONObject(selected).getString("lon")

                                // Save all needed values
                                settings.edit()
                                    .putString("lon", lon)
                                    .putString("lat", lat)
                                    .putString("city", city)
                                    .putString("api", api)
                                    .apply()

                                startMain(true)
                            }
                            .setNegativeButton(R.string.abort) { dialog, _ ->
                                dialog.cancel()
                                binding.loader.visibility = View.GONE
                                binding.mainContainer.visibility = View.VISIBLE
                            }
                            .setCancelable(false)
                            .show()
                    } else {
                        binding.loader.visibility = View.GONE
                        binding.mainContainer.visibility = View.VISIBLE
                        when (error) {
                            1 -> {
                                Toast.makeText(
                                    this@StartActivity,
                                    R.string.error_1,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            0, 2 -> {
                                Toast.makeText(
                                    this@StartActivity,
                                    R.string.error_2,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            else -> {
                                Toast.makeText(
                                    this@StartActivity,
                                    R.string.error_3,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun startMain(mustSuccess: Boolean) {
        // Load new data
        val executor = Executors.newScheduledThreadPool(5)

        doAsync(executorService = executor) {
            val result: String? = try {
                with(URL(gf.url("normal", "")).openConnection() as HttpsURLConnection) {
                    sslSocketFactory = gf.getSocketFactory()
                    requestMethod = "GET"
                    BufferedReader(InputStreamReader(inputStream)).readText()
                }
            } catch (e: Exception) {
                null
            }
            uiThread {
                if (result != null || !mustSuccess) { // when loaded data isn't empty, or old data is available, start the Main activity
                    if (result != null) {
                        gf.setResult(result)
                        gf.setInitialized(true)
                    } else {
                        Toast.makeText(
                            this@StartActivity,
                            R.string.error_occurred,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    val intent = Intent(this@StartActivity, MainActivity::class.java)
                    startActivity(intent)
                    WidgetProvider().updateWidgets(applicationContext)
                    finish()
                } else { // else display error message
                    Toast.makeText(this@StartActivity, R.string.error_3, Toast.LENGTH_LONG).show()
                    binding.loader.visibility = View.GONE
                    binding.mainContainer.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun changeSettings() {
        val settings: SharedPreferences = getSharedPreferences("de.beowulf.wetter", 0)
        val inflater = layoutInflater
        val alertLayout: View = inflater.inflate(R.layout.settings, null)
        val alert = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialog))
        //Get StringArrays
        val unitsTemp = resources.getStringArray(R.array.unitsTemp)
        val unitsSpeed = resources.getStringArray(R.array.unitsSpeed)
        val unitsDistance = resources.getStringArray(R.array.unitsDistance)
        val themes = resources.getStringArray(R.array.Themes)
        //Load current settings
        var unitTemp: Int = settings.getInt("unitTemp", 0)
        var unitSpeed: Int = settings.getInt("unitSpeed", 0)
        var unitDistance: Int = settings.getInt("unitDistance", 0)
        var theme: Int = settings.getInt("Theme", 1)
        var gradient = settings.getBoolean("Gradient", true)
        var timeFormat: Boolean = settings.getBoolean("24hTime", false)
        //initialize elements
        val unitTempSpinner = alertLayout.findViewById<Spinner>(R.id.UnitTemp)
        val unitSpeedSpinner = alertLayout.findViewById<Spinner>(R.id.UnitSpeed)
        val unitDistanceSpinner = alertLayout.findViewById<Spinner>(R.id.UnitDistance)
        val themeSpinner = alertLayout.findViewById<Spinner>(R.id.Theme)
        val gradientCheckbox = alertLayout.findViewById<CheckBox>(R.id.Gradient)
        val timeFormatCheckbox = alertLayout.findViewById<CheckBox>(R.id.TimeFormat)
        //set current settings
        unitTempSpinner.adapter = ArrayAdapter(this, R.layout.spinner, unitsTemp)
        unitTempSpinner.setSelection(unitTemp)
        unitSpeedSpinner.adapter = ArrayAdapter(this, R.layout.spinner, unitsSpeed)
        unitSpeedSpinner.setSelection(unitSpeed)
        unitDistanceSpinner.adapter = ArrayAdapter(this, R.layout.spinner, unitsDistance)
        unitDistanceSpinner.setSelection(unitDistance)
        themeSpinner.adapter = ArrayAdapter(this, R.layout.spinner, themes)
        themeSpinner.setSelection(theme)
        gradientCheckbox.isChecked = gradient
        timeFormatCheckbox.isChecked = timeFormat
        //set AlertView
        alert.setView(alertLayout)
            .setCancelable(false)
            .setNegativeButton(R.string.abort) { dialog, _ ->
                dialog.cancel()
            }
            .setPositiveButton(R.string.save) { dialog, _ ->
                unitTemp = unitTempSpinner.selectedItemPosition
                unitSpeed = unitSpeedSpinner.selectedItemPosition
                unitDistance = unitDistanceSpinner.selectedItemPosition
                timeFormat = timeFormatCheckbox.isChecked
                settings.edit()
                    .putInt("unitTemp", unitTemp)
                    .putInt("unitSpeed", unitSpeed)
                    .putInt("unitDistance", unitDistance)
                    .putBoolean("24hTime", timeFormat)
                    .apply()
                if (theme == themeSpinner.selectedItemPosition &&
                        gradient == gradientCheckbox.isChecked) {
                    dialog.cancel()
                } else {
                    theme = themeSpinner.selectedItemPosition
                    gradient = gradientCheckbox.isChecked
                    settings.edit()
                        .putInt("Theme", theme)
                        .putBoolean("Gradient", gradient)
                        .apply()
                    finish()
                    startActivity(intent)
                }
            }
            .show()
    }
}