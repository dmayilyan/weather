package de.beowulf.wetter

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import de.beowulf.wetter.databinding.ActivityMapBinding

class MapActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMapBinding

    private val gf = GlobalFunctions()

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(gf.getTheme(this))
        super.onCreate(savedInstanceState)
        binding = ActivityMapBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gf.initializeContext(this)

        binding.MapView.settings.javaScriptEnabled = true
        binding.MapView.loadUrl(gf.url("Map", ""))
        binding.MapView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                    setOverlay(binding.navBar.selectedItemId)
            }
        }

        binding.navBar.setOnNavigationItemSelectedListener { item ->
            val i = item.itemId
            setOverlay(i)
            true
        }
    }

    private fun setOverlay(item: Int) {
        when (item) {
            R.id.map_temp -> binding.MapView.loadUrl(
                ("javascript:map.removeLayer(cloudsLayer);map.removeLayer(windLayer);map.removeLayer(rainLayer);"
                        + "map.addLayer(tempLayer);")
            )
            R.id.map_rain -> binding.MapView.loadUrl(
                ("javascript:map.removeLayer(cloudsLayer);map.removeLayer(windLayer);map.removeLayer(tempLayer);"
                        + "map.addLayer(rainLayer);")
            )
            R.id.map_wind -> binding.MapView.loadUrl(
                ("javascript:map.removeLayer(cloudsLayer);map.removeLayer(rainLayer);map.removeLayer(tempLayer);"
                        + "map.addLayer(windLayer);")
            )
            R.id.map_clouds -> binding.MapView.loadUrl(
                "javascript:map.removeLayer(rainLayer);map.removeLayer(windLayer);map.removeLayer(tempLayer);"
                        + "map.addLayer(cloudsLayer);"
            )
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}