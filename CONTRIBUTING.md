# Contributing

## Reporting issues

If you find an issue in the client, you can use our [Issue
Tracker](https://gitlab.com/BeowuIf/wetter/-/issues). Make sure that it
hasn't yet been reported by searching first.

Remember to include the following information:

* Android version
* Device model
* App version
* Steps to reproduce the issue

## Translating

You can translate the strings directly in Android Studio [(more Infos)](https://gitlab.com/BeowuIf/weather/-/wikis/Translation) or online at [POEditor](https://poeditor.com/join/project?hash=WhLrkxFJUt).
